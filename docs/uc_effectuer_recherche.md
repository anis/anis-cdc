# Use case : Effectuer une recherche dans un projet

## Acteur principal

L'utilisateur

## Objectifs

L'utilisateur veut pouvoir effecuer une recherche dans un projet scientifique

## Précondition

 * L'utilisateur a sélectionné un projet scientifique

## Scenario nominal

1. L'utilisateur clique sur le bouton `Search data` dans la barre de menu
2. L'utilisateur sélectionne le jeux de données dans lequel il veut faire une recherche
3. Le système lui retourne les critères et les colonnes de sortie par défaut
4. L'utilisateur clique sur `Criteria`
5. Le système propose à l'utilisateur de remplir 0 ou plusieurs critères de recherche (voir `uc criteria` ou `uc cone-search`)
6. L'utilisateur clique sur `Output`
7. Le système propose à l'utilisateur de sélectionner ou déselectionner des colonnes de sortie
8. L'utilisateur clique sur `Result`
9. Le système présente à l'utilisateur le résultat de sa recherche avec le nombre d'enregistrements trouvés

## Alternative

9a - Le système préviens l'utilisateur qu'il n'a pas trouvé d'enregistrements pour cette recherche.
La page de résultat n'est donc pas affichée.
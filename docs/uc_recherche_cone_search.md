# Use case : Recherche par cone-search

## Acteur principal

L'utilisateur

## Objectifs

L'utilisateur veut rechercher dans un jeux de données par cone-search (position spatiale + rayon)

## Précondition

 * L'utilisateur a sélectionné un projet scientifique
 * L'utilisateur a selectionné un jeux de données sur lequel il veut effectuer une recherche
 * L'itilisateur est sur l'onglet `Criteria`

## Scenario nominal

1. L'utilisateur entre une valeur dans la propriété `RA` comprise entre 0 et 360
2. L'utilisateur entre une valeur dans la propriété `DEC` comprise entre -90 et 90
3. L'utilisateur choisi un rayon de recherche
4. L'utilisateur peut alors cliquer sur le bouton `+` pour valider `son cone-search`

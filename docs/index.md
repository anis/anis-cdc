# AstroNomical Information System - Présentation

![anis_v3_logo](img/anis_v3_logo300.png#center)

## Cadre du projet

### Résumé du projet

ANIS est un système de gestion de contenu générique permettant la mise à disposition de
données pour des projets scientifiques dans le domaine de l’astrophysique.

Les objectifs d’ANIS sont :

 * La recherche, la visualisation ou l’extraction de données issues de différents projets
scientifiques
 * La configuration des différents projets scientifiques via une interface web
(dashboard d’administration)
 * La mise à disposition d’un portail pour lister et accéder aux différents projets
scientifiques

### Contexte

ANIS est un projet d'application web en R&D du Laboratoire d’Astrophysique de Marseille qui est développé par
l’équipe des systèmes d’informations. 

La thématique SI est une des quatres thématiques du CeSAM, le Centre de donnéeS Astrophysique de Marseille.

### Enjeux et objectifs

Dans un souci d'homogénéisation et de standardisation d'accès aux différentes données du
CeSAM, l'équipe CeSAM-SI a pris la décision d'élaborer un logiciel de mise à disposition et de
visualisation des données scientifiques. Ce logiciel doit avoir pour but de fournir une
plate-forme complète pour la gestion des différents systèmes d’informations hébergés au
Laboratoire d’Astrophysique de Marseille.

### Livrables

L’ensemble du code source est disponible sur le gitlab du LAM à l’adresse suivante :
[https://gitlab.lam.fr/anis](https://gitlab.lam.fr/anis).

ANIS est fourni sous la forme d’images docker disponibles sur le registry du gitlab à
l’adresse suivante : `registry.lam.fr`.

### Équipe de développement

L’équipe de développement est composé de la responsable de la thématique systèmes
d’informations `Chrystel Moreau` qui a le rôle de commanditaire ainsi que d’un chef de projet
technique `François Agneray` qui a le rôle d’expert chargé de proposer et de mettre en œuvre
une réponse technique adaptée aux besoins.
L’équipe est également composée d’une développeuse, spécialisée dans la partie frontend,
`Tifenn Guillas`.

### Licence

Le projet sera en open source avec une licence libre CeCIL : [http://www.cecill.info/](http://www.cecill.info/).

## Fonctionnalités de l'application

### Portail

Le portail est la racine de l’application. L’utilisateur peut parcourir les projets scientifiques
disponibles. Il pourra ensuite choisir un projet dans lequel il veut effectuer une recherche

### Connexion

L'utilisateur aura la possibilité de se connecter sur son compte utilisateur. Il pourra également créer un compte
utilisateur si il n'en posséde pas.

### Recherche

Une fois un projet scientifique sélectionné, l’utilisateur pourra effectuer une recherche. Pour
ce faire, il devra :

 * Sélectionner un jeu de données dans lequel il veut effectuer une recherche
 * Saisir un critère ou même plusieurs critères à la fois
 * Il pourra sélectionner les informations à afficher (colonnes de sortie)
 * Enfin, les résultats de la recherche seront disponibles sur une page particulière et devront pouvoir être parcourus

### Détail

Sur la page de résutat, l’utilisateur aura la possibilité de sélectionner un enregistrement
pour afficher une page de détail d'un objet. Cette page pourra afficher des données complémentaires ou des visualisations
particulières (spectres, images, …).

### Téléchargement

Sur la page de résutat, l’utilisateur aura la possibilité de télécharger l’ensemble des résultats d’une recherche dans
différents formats.

### Sélection

Sur la page de résutat, l’utilisateur aura la possibilité de sélectionner une partie du résultat de la recherche pour
effectuer des actions comme par exemple le téléchargement.

### Sauvegarder la recherche

Sur la page de résutat, l'utilisateur aura la possibilité de sauvegarder l’état de sa recherche pour y revenir
ultérieurement.

### Dashboard d’administration

Un administrateur aura la possibilité d’ajouter des projets scientifiques, des jeux de données
disponibles via une interface d’administration. Il pourra configurer les formulaires de
recherche ainsi que la visualisation des résultats. Il pourra également gérer les droits
d'accès sur les jeux de données.

De plus, l'administrateur pourra configurer le rendu des pages des résultats, des pages de détails, 
activer ou désactiver des fonctionnalités.

## Spécifications techniques

### Choix technologiques

Côté client nous avons choisi d’utiliser le framework `Angular` qui est open source basé sur
TypeScript et qui permet la création d’applications Web et plus particulièrement de ce qu’on
appelle des « Single Page Applications » : des applications web accessibles via une page
web unique qui permet de fluidifier l’expérience utilisateur et d’éviter les chargements de
pages à chaque nouvelle action.

Côté serveur nous avons choisi d’utiliser `PHP` qui est un langage de programmation libre
possédant un moteur objet puissant et qui permet de manipuler facilement le protocole
HTTP. Nous avons choisi d’utiliser le micro framework libre slim-php qui met à disposition
du développeur un ensemble de fonctionnalités pour manipuler les requêtes et les réponses
HTTP. C'est le serveur qui permet d'interroger les bases de données.

Nous avons également décidé d’utiliser `Python` et la bibliothèque `Astropy` pour des
fonctionnalités métiers liées à l’astronomie et l’astrophysique.

Pour la gestiion de l'authentification et de la gestion des utilisateurs nous avons décidé d'intérfacer
ANIS avec le logiciel `Keycloak`.

### Domaine et hébergement

Une version de ANIS doit être installée au LAM pour la gestion des différents systèmes
d’informations dont elle a la responsabilité. Cette version sera installée sur l'infrastructure du
service CeSAM et l’adresse du public sera [https://cesamsi.lam.fr](https://cesamsi.lam.fr).

###  Site web et documentation

Un site web est disponible à l’adresse suivante : [https://anis.lam.fr](https://anis.lam.fr). Il contient des
informations sur le projet ANIS comme par exemple, les différentes versions de
développement, les fonctionnalités actuelles ou à venir, les publications autour de la
thématique système d’information, ect…

Nous mettons également à disposition une documentation technique à l’adresse suivante:
[https://anis.lam.fr/doc/](https://anis.lam.fr/doc/). Cette documentation technique est destinée aux développeurs et/ou
aux personnes désirant installer une version d’ANIS sur leur infrastructure.
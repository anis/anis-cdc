# Use case : Sauvegarder la recherche

## Acteur principal

L'utilisateur connecté

## Objectifs

 * L'utilisateur veut pouvoir sauvegarder l'état de sa recherche pour pouvoir y revenir utérieurement

## Précondition

 * L'utilisateur doit se connecter
 * L'utilisateur doit avoir effectuer une recherche et se trouver sur la page de résultat

## Scenario nominal

1. L'utilisateur clique sur le bouton sauvegarder cette recherche dans la page de résultat
2. Le système demande à l'utilisateur de donner un nom à cette recherche
3. L'utilisateur entre un nom pour sa recherche et confirme la sauvegarde
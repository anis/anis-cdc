# Les cas d'utilisations

## Présentation

Ici vous pouvez trouver le diagramme des cas d'utilisations que nous détaillerons au fil de cette documentation.

 * Les cas d'utilisations sur fond blanc sont ceux qui sont déjà implementés dans le logiciel.
 * Les cas d'utilisations sur fond gris sont ceux qui ont été identifiés mais par encore implementés.

## Diagramme

![cas_utilisations.png](img/cas_utilisations.png#center)

## Les acteurs

- L'utilisateur : Ce rôle représente l'utilisateur (non connecté) de l'application. Il peut uniquement accéder aux données publiques.
- L'utilisateur connecté : Ce rôle représente l'utilisateur connecté de l'application. Il peut accéder aux données publiques + à certaines données privées.
- L'administateur : Ce côle représente un utilisateur connecté sur l'application avec un pouvoir d'administration.
Il pourra donc effectuer des actions de configuration de l'application
# Use case : Visualiser le détail d'un enregistrement

## Acteur principal

L'utilisateur

## Objectifs

 * L'utilisateur veut pouvoir visualiser le détail d'un enregistrement

## Précondition

 * L'utilisateur doit avoir effectuer une recherche et se trouver sur la page de résultat
 * L'utilisateur doit parcourir les données dans la table des données
 * L'administrateur doit avoir configurer une page de détail pour le jeux de données

## Scenario nominal

1. L'utilisateur parcour les données dans la table des données et clique sur l'identifiant de l'enregistrement sur lequel il veut
 plus d'informations
2. Le système redirige l'utilisateur vers la page de détail de cet enregistrement
3. L'utilisateur peut prendre connaissance du détail de l'enregistrement et il à la possibilité de revenir sur la page de résultat
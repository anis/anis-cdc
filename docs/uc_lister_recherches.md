# Use case : Lister les recherches sauvegardées

## Acteur principal

L'utilisateur connecté

## Objectifs

 * L'utilisateur veut pouvoir lister l'ensemble des recherches qu'il a déjà sauvegardées
 * L'utilisateur veut pouvoir rejouer une recherche dans sa liste

## Précondition

 * L'utilisateur doit se connecter

## Scenario nominal

1. L'utilisateur clique sur l'îcone de profil utilisateur en haut à droite de la barre de menu pour faire apparaître le sous-menu utilisateur
2. Il clique sur `My saved searches`
3. Le système retourne la liste des recherches sauvegardées pour l'utilisateur 
4. L'utilisateur clique sur une recherche sauvegardée
5. Le système redirige l'utilisateur sur la page de résultat pour cette recherche sauvegardée
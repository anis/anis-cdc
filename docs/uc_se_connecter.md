# Use case : Se connecter

## Acteur principal

L'utilisateur

## Objectifs

L'utilisateur veut pouvoir se connecter sur l'application pour 
augmenter son niveau d'autorisation.

## Scenario nominal

1. L'utilisateur clique sur le bouton `Sign in / Register` en haut à droite de la barre de menu
2. L'utilisateur est redirigé vers l'application `Keycloak` qui lui demande son email et son mot de passe
3. L'utilisateur entre son email et son mot de passe et clique sur `Sign In`
4. Le système `Keycloak` vérifie le couple email + mot de passe et retourne un jeton de connexion
5. Le système renvoie l'utilisateur, maintenant connecté, vers l'application ANIS 
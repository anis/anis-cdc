# Use case : Recherche par criteria

## Acteur principal

L'utilisateur

## Objectifs

 * L'utilisateur veut rechercher dans un jeux de données par critères 
 * L'utilisateur peut ajouter plusieurs critères à sa recherche

## Précondition

 * L'utilisateur a sélectionné un projet scientifique
 * L'utilisateur a selectionné un jeux de données sur lequel il veut effectuer une recherche
 * L'itilisateur est sur l'onglet `Criteria`

## Scenario nominal



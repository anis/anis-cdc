# Use case : Effectuer une action sur une sélection

## Acteur principal

L'utilisateur

## Objectifs

 * L'utilisateur veut pouvoir effectuer une action sur une sélection d'enregistrements

## Précondition

 * L'utilisateur doit avoir effectuer une recherche et se trouver sur la page de résultat
 * L'utilisateur doit parcourir les données dans la table des données
 * L'administrateur doit avoir configurer la possibilité de sélectionner des enregistrements pour ce jeux de données

## Scenario nominal

1. L'utilisateur sélectione 1 ou plusieurs enregistrements grâce aux cases à cocher dans la premiere colonne du tableau des résultats
2. Le système autorise alors l'utilisateur à choisir une action possible sur cette sélection
3. L'utilisateur choisi dans une liste déroulante l'action qu'il veut faire pour cette sélection (télécharger, afficher sous forme de graphoqique, ect...)
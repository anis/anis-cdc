# Use case : Télécharger les données de la recherche

## Acteur principal

L'utilisateur connecté

## Objectifs

 * L'utilisateur veut pouvoir télécharger le contenu de la recherche (données + fichiers associés)

## Précondition

 * L'utilisateur doit se connecter
 * L'utilisateur doit avoir effectuer une recherche et se trouver sur la page de résultat

## Scenario nominal

 1. L'utilisateur clique sur télécharger la recherche
 2. Le système confirme à l'utilisateur que sa demande à bien été prise en compte et préviendra 
 l'utilisateur lorsque le téléchargement sera possible
 3. Le système génère l'archive côté serveur et envoie un email à l'utilisateur pour le prévenir de la disponibilité de l'archive
 4. L'utilisateur peut récupérer l'archive soit en cliquant sur le lien dans l'email ou via son espace personnel sur l'application ANIS


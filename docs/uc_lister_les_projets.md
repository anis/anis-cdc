# Use case : Lister les projets scientifiques

## Acteur principal

L'utilisateur

## Objectifs

L'utilisateur veut pouvoir consulter les différents projets scientifiques disponibles sur la plate-forme ANIS.

## Scenario nominal

1. L'utilisateur se connecte sur la page racine de l'application ANIS
2. Le système lui retourne la liste des projets scientifiques disponibles
3. L'utilisateur peut sélectionner un projet et ainsi effectuer une recherche
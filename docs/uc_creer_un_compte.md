# Use case : Créer un compte utilisateur

## Acteur principal

L'utilisateur

## Objectifs

L'utilisateur veut pouvoir créer un compte utilisateur pour pouvoir se connecter

## Scenario nominal

1. L'utilisateur clique sur le bouton `Sign in / Register` en haut à droite de la barre de menu
2. L'utilisateur est redirigé vers l'application `Keycloak` sur le formulaire de connexion
3. L'utilisateur peut cliquer sur le bouton `Register` pour demander la création d'un nouveau compte
4. L'utilisateur est redirigé vers le formulaire de demande de nouveau compte
5. L'utilisateur rempli le formulaire et clique sur le bouton `Register`
6. Le système `Keycloak` envoie un email de vérification
7. L'utilisateur clique sur le lien dans l'email pour valider la création de son compte utilisateur
8. Le système valide la création et renvoie l'utilisateur, maintenant connecté, vers l'application ANIS
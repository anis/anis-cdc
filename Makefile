UID := $(id -u)
GID := $(id -g)
NAME_APP=mkdocs

list:
	@echo ""
	@echo "Useful targets:"
	@echo ""
	@echo "  install      > build mkdocs docker image"
	@echo "  start        > run a dev server for $(NAME_APP) application (in memory)"
	@echo "  stop         > stop the dev server for $(NAME_APP) application"
	@echo "  restart      > restart the dev server for $(NAME_APP) (container)"
	@echo "  status       > display $(NAME_APP) container status"
	@echo "  build        > generate dist application (html, css, js)"
	@echo "  logs         > display $(NAME_APP) container logs"
	@echo "  shell        > shell into $(NAME_APP) container"
	@echo ""

install:
	@docker build -f ./Dockerfile.dev -t mkdocs-img . 

start:
	@docker run --init -it --rm \
	--user $(UID):$(GID) \
	--name $(NAME_APP) \
	-p 8888:8888 \
	-v $(CURDIR):/project  \
	-w /project -d mkdocs-img mkdocs serve

stop:
	@docker stop $(NAME_APP)

restart: stop start

status:
	@docker ps -f name=$(NAME_APP)

build:
	@docker run --init -it --rm \
	--user $(UID):$(GID) \
	-v $(CURDIR):/project  \
	-w /project mkdocs-img mkdocs build --clean

logs:
	@docker logs -f -t $(NAME_APP)

shell:
	@docker exec -ti $(NAME_APP) bash
